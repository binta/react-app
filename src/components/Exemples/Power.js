import React from 'react'
import {Button} from "@mui/material";
import { useDispatch } from 'react-redux';
import { upPower } from '../../Stores/PowerSlice';
export default function Power() {
  
  const dispatch = useDispatch();


  return (
    <Button
    variant={'contained'}
    color={'secondary'}
    onClick={() =>{
      dispatch(upPower());
    }}>Cliquer</Button>
  )
}
