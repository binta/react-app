import {AppBar, Box, Button, Dialog, DialogContent, DialogTitle, Stack, Toolbar,Typography} from "@mui/material";
import {Link, useNavigate} from "react-router-dom";
import {SwitchTheme} from "./SwitchTheme";
import {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {LoginForm} from "../Auth/LoginForm";
import {BASE_PATH} from "../../App";
import jwtDecode from "jwt-decode";


export function TopNav(props) {
    const {onThemeModeChanged} = props;
    const storeCounterValue = useSelector(state => state.counter.value);
    const power = useSelector(state => state.power.value);
    const [dialogLoginOpen, setDialogLoginOpen] = useState(false);
    const [user, setUser] = useState(null);

    const connectUser = () => {
        const token = sessionStorage.getItem('token');

        if (token) {
            const decodedUser = jwtDecode(token);
            setUser(decodedUser);
        }
    }

    useEffect(() => {
        connectUser();
    }, [])


    const navigate = useNavigate();
    const token = sessionStorage.getItem('token');
    
    return (
        <>
            <Box sx={{flexGrow: 1}}>
                <AppBar position="static">
                    <Toolbar>
                        <Stack direction={'row'} justifyContent={'space-between'} sx={{width: '100%'}}
                               alignItems={'center'}>
                            <Stack direction={'row'} gap={2}>
                                <Link to={BASE_PATH + '/'} style={{color: 'white', textDecoration: 'none'}}>Voyages</Link>
                                <Link to={BASE_PATH + '/compteur'} style={{color: 'white', textDecoration: 'none'}}>Compteur</Link>
                                <Link to={BASE_PATH + '/power'} style={{color: 'white', textDecoration: 'none'}}>Puissance de 2</Link>

                                {
                                    token
                                        ? <Link to={'/admin/trip'} style={{color: 'white', textDecoration: 'none'}}>
                                            Back office
                                        </Link>
                                        : ''
                                }

                            
                            
                            </Stack>
                            <Typography>{storeCounterValue}</Typography>
                            <Typography>{power}</Typography>
                            <Stack direction={'row'} gap={2}>
                                <SwitchTheme onThemeModeChanged={onThemeModeChanged}/>
                                {
                                    !user &&(
                                        <Button
                                  color="inherit"
                                  onClick={() => {
                                      setDialogLoginOpen(true)
                                  }}
                              >Administration</Button>
                                    )
                                  
                                }
                                
                            </Stack>
                        </Stack>
                        <Stack>
                        {
                                user && (
                                    <>
                                        <Typography sx={{color: 'white'}}>{user.email}</Typography>
                                        <Button onClick={() => {
                                            sessionStorage.removeItem('token');
                                            setUser(null);
                                        }} color={'secondary'}>Déconnexion</Button>
                                    </>

                                )
                            }

                        </Stack>
                    </Toolbar>
                   
                </AppBar>
            </Box>
            <Dialog open={dialogLoginOpen} onClose={() => {
                setDialogLoginOpen(false)
            }}>
                
                <DialogTitle>
                    Connexion
                </DialogTitle>
                <DialogContent>
                    <LoginForm handleLoginSuccess={() => {
                        setDialogLoginOpen(false);
                        connectUser();
                        navigate(BASE_PATH + '/admin/trip');
                    }}/>
                </DialogContent>
            </Dialog>
        </>
    );
}



 
 