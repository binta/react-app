import {useEffect, useRef, useState} from "react";
import Container from "../Layout/Container";
import {Box, Button, Stack, TextField} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {incrementCounter, upCounter} from "../../Stores/CounterSlice";

export default function Counter(props) {
    // store
    const cpt = useSelector (state => state.counter.value);
    const dispatch = useDispatch();

    // component
    const ref = useRef(null);
    const style = props.style;
    const [userValue, setUserValue] = useState('');

    // console.log('Execution de Counter');

    // "useEffect" s'éxecutera une seule fois
    useEffect(
        () => {
            // console.log('Execution du useEffect');
        },
        []
    )

    const upCpt = () => {
        dispatch(incrementCounter());
        // console.log('test du p : ' + ref.current.innerText);
    }

    return (
        <Container>
            <Box sx={style}>
                <Button color={'secondary'} variant={'contained'} className='btn' onClick={upCpt}>Augmenter la valeur du
                    compteur</Button>
                <p ref={ref}>Le compteur vaut : {cpt}</p>
            </Box>
            <Stack>
                <TextField
                    type={'number'}
                    label={'Valeur'}
                    value={userValue}
                    onChange={e => {setUserValue(e.target.value)}}
                />
                <Button variant={'contained'} onClick={() => {
                    dispatch(upCounter(userValue));
                }}>
                    Mettre à jour le compteur
                </Button>
            </Stack>

        </Container>
    )
}



