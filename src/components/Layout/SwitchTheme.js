import {Box, Switch} from "@mui/material";

export function SwitchTheme(props) {
    const {onThemeModeChanged} = props;

    return (
        <Box>
            <Switch
                color={'secondary'}
                onClick={onThemeModeChanged}
            />
        </Box>
    )
}
