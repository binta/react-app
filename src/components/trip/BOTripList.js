import {Container, Paper, Table, TableBody, TableCell, TableHead, TableRow, Typography, Button} from "@mui/material";
import {useEffect, useState} from "react";
import {TripApiService} from "../../Services/Trip/TripApiService";
import TripLine from "./TripLine";
import {Link, useNavigate} from "react-router-dom";

export function BOTripList() {

    // Définition le state qui contiendra les voyages
    const [trips, setTrips] = useState(null);

    // On charge les voyages une seule fois (tableau vide en 2eme paramètre)
    useEffect(() => {
        TripApiService
            .findAllTrips()
            .then(res => {
                console.log("Réponse de l'API (bo trips)", res);
                // On récupère les voyages
                const apiTrips = res.data;
                // On stocke les voyages dans le state
                setTrips(apiTrips);
            })
    }, []);

    // On récupère le token pour vérifier que l'utilisateur est connecté
    const token = sessionStorage.getItem('token');

    // Si le token n'est pas présent, on affiche pas la page
    if (!token) {
        return (
            <Container>
                <Typography variant={'h1'}>Accès interdit</Typography>
            </Container>
        )
    }

    // S'il n'y a pas de voyages (=== null) c'est que le call API est en cours
    if (!trips) {
        return <Container>
            Chargement en cours
        </Container>
    }

    // A ce stage, on a les voyages
    return (
        <Container>
            {
                trips.length > 0 && (
                    <>
                        <h1>Les voyages</h1>
                        <Link to={'/admin/trip/new'}>
                            <Button variant="text" color={'success'}>
                            Ajouter un voyage
                            </Button>
                        </Link>
                        <Paper>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Destination</TableCell>
                                        <TableCell>Durée</TableCell>
                                        <TableCell>Actions</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {
                                        trips.map(elem => <TripLine key={'trip-' + elem.id} trip={elem}/>)
                                    }
                                </TableBody>
                            </Table>
                        </Paper>
                    </>
                )
            }
        </Container>
    );
}
