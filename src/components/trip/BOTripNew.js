import {Button, Container, Paper, Stack, TextField, Typography} from "@mui/material";
import {useState} from "react";
import {TripApiService} from "../../Services/Trip/TripApiService";
import {useNavigate} from "react-router-dom";

export function BOTripNew() {
    const [trip, setTrip] = useState({
        country: '',
        duration: '',
        city: '',
        beginAt: '',
        endAt: '',
        isFinished: '',
    });
    const navigate = useNavigate();

    return (
        <Container>
            <Typography variant={'h1'} sx={{mt: 2}}>Ajout d'un voyage</Typography>

            <Paper sx={{my: 2, p: 2}}>
                <form
                    onSubmit={event => {
                        // On empêche la soumission du formulaire
                        event.preventDefault();

                        TripApiService
                            .addTrip(trip)
                            .then(res => {
                                navigate('/admin/trip/' + res.data.id)
                            })
                        ;
                    }}
                >
                    <Stack gap={2}>
                        <TextField label={'Ville'} value={trip.city} onChange={event => {
                            const value = event.target.value;
                            setTrip({
                                ...trip,
                                city: value
                            })
                        }}/>
                        <TextField label={'Pays'} value={trip.country} onChange={event => {
                            const value = event.target.value;
                            setTrip({
                                ...trip,
                                country: value
                            })
                        }}/>
                        <TextField label={'Durée du voyage'} value={trip.duration} onChange={event => {
                            const value = event.target.value;
                            setTrip({
                                ...trip,
                                duration: value
                            })
                        }}/>

                        <Button variant={'outlined'} color={'primary'} type={'submit'}>Ajouter le voyage</Button>
                    </Stack>
                </form>
            </Paper>
        </Container>
    )
}
