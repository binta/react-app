import {TableCell, TableRow} from "@mui/material";

import {Link} from "react-router-dom";
import VisibilityIcon from '@mui/icons-material/Visibility';

function TripLine(props) {
    const trip = props.trip;
    
    return(
        <TableRow>
            <TableCell>{trip.country}</TableCell>
            <TableCell>{trip.duration}</TableCell>
            <TableCell>
                <Link to={'/admin/trip/' + trip.id}>
                    <VisibilityIcon color={'primary'}/>
                </Link>
            </TableCell>
        </TableRow>

    )
}
export default TripLine;