
import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import {TripApiService} from "../../Services/Trip/TripApiService";
import {Card, CardContent, Container, Typography} from "@mui/material";

/**
 * {
 * 	"data": {
 * 		"id": 6,
 * 		"country": "France",
 * 		"duration": "2 jours",
 * 		"city": "Paris",
 * 		"beginAt": null,
 * 		"endAt": null,
 * 		"isFinished": false
 * 	}
 * }
 */

export function BOTripShow() {

    const [trip, setTrip] = useState(null);
    const params = useParams();

    useEffect(() => {
        TripApiService
            .find(params.id)
            .then(res => {
                const apiTrip = res.data;
                setTrip(apiTrip);
            })
        ;
    }, []);

    if (!trip) {
        return <Container>
            <Typography>Chargement en cours</Typography>
        </Container>
    }

    return (
        <Container>
            <Card sx={{mt: 2}}>
                <CardContent>
                    <Typography variant={'h4'}>{trip.city} - {trip.country}</Typography>
                </CardContent>
                <CardContent>
                    <Typography variant={'body1'}>{trip.duration}</Typography>
                </CardContent>
                {
                    !trip.isFinished && (
                        <CardContent>
                            <Typography variant={'body1'}>Voyage en cours</Typography>
                        </CardContent>
                    )
                }
            </Card>
        </Container>
    )
}
