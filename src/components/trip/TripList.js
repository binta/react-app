import {useLastTrips} from "../../Hooks/useLastTrips";
import Container from "../Layout/Container";
import TripLine from "./TripLine";
import {Paper} from "@mui/material";


export function TripList() {

  const trips = useLastTrips()
  if (!trips) {
    return (<Container>
      chargement en cours
    </Container>)
  }

  return (
      <Container>
        <h1>Les voyages</h1>
        <Paper>
          <table>
            <thead>
            <tr>
              <td>Destination</td>
              <td>Durée</td>
            </tr>
            </thead>
            <tbody>
            {
              trips.map(elem => <TripLine key={'trip=' + elem.id} trip={elem}/>)
            }
            </tbody>
          </table>
        </Paper>
      </Container>
  )
}