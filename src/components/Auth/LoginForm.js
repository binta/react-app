import React, {useState} from 'react';
import {Alert, Box, Button, TextField} from "@mui/material";
import {TripApiService} from "../../Services/Trip/TripApiService";
import jwtDecode from 'jwt-decode';

export function LoginForm(props) {
    const {handleLoginSuccess} = props;

    const [email, setEmail] = React.useState('admin@gmail.com');
    const [password, setPassword] = useState('admin@dm1N');
    const [errorMessage, setErrorMessage] = useState('');

    function handleSubmit(event) {
        // On empêche l'envoi du formulaire
        event.preventDefault();

        TripApiService
            .login(email, password)
            .then(res => {
                const token = res?.data?.token;

                if (!token) {
                    // affichage du message d'erreur
                    setErrorMessage("Les identifiants saisis sont invalides");
                    return;
                }

                // stockage du token
                sessionStorage.setItem('token', token);
                const user = jwtDecode(token);
                console.log('user',user);



                // redirection vers le back-office
                handleLoginSuccess();
            })
        ;
    }

    return (
        <form onSubmit={handleSubmit}>
            {
                errorMessage.length > 0 && <Alert severity="error">{errorMessage}</Alert>
            }

            <Box my={2}>
                <TextField label={'Email'} value={email} onChange={event => {
                    setEmail(event.target.value);
                }}/>
            </Box>
            <Box my={2}>
                <TextField type={'password'} label={'Mot de passe'} value={password} onChange={e => {
                    setPassword(e.target.value);
                }}/>
            </Box>
            <Button variant={"contained"} type={'submit'}>Se connecter</Button>
        </form>
    )
}
