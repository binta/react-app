import React, {useState} from 'react'
import './App.css';
import {TopNav} from "./components/Layout/TopNav";
import {Container, CssBaseline, ThemeProvider} from "@mui/material";
import {getTheme} from "./Shared/Theme/themeOptions";
import {BrowserRouter, Routes, Route} from "react-router-dom";
import {TripList} from "./components/Trip/TripList";
import Counter from "./components/Layout/Counter";
import {BOTripList} from "./components/Trip/BOTripList";
import Power from './components/Exemples/Power';
import { BOTripShow } from './components/Trip/BOTripShow';
import {BOTripNew} from './components/Trip/BOTripNew';

export const BASE_PATH = '';

function App() {
    const [themeMode, setThemeMode] = useState('light');

    return (
        <React.StrictMode>
            <ThemeProvider theme={getTheme(themeMode)}>
                <CssBaseline/>
                <BrowserRouter>
                    <Container>
                        <TopNav onThemeModeChanged={() => {setThemeMode(themeMode === 'light' ? 'dark' : 'light')}}/>
                        <main>
                            <Routes>
                                <Route path={ '/'} element={<TripList/>}/>
                                <Route path={'/compteur'} element={<Counter/>}/>
                                <Route path={'/admin/trip'} element={<BOTripList/>}/>
                                <Route path={'/admin/trip/new'} element={<BOTripNew/>}/>
                                <Route path={'/admin/trip/:id'} element={<BOTripShow/>}/>
                                <Route path={'/power'} element={<Power/>}/>
                            </Routes>
                        </main>
                    </Container>
                </BrowserRouter>
            </ThemeProvider>
        </React.StrictMode>
    )
}

export default App;
