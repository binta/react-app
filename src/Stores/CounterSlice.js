import {createSlice} from "@reduxjs/toolkit";
export const counterSlice = createSlice({
 name: 'counter',
 initialState: {value: 2},
 reducers: {
 incrementCounter: state => {
 state.value = state.value + 1
 },
 upCounter :(state,action)=> {
     const paylod = Number(action.payload);
     state.value = state.value + paylod;
 }
 }
});
export const {incrementCounter,upCounter} = counterSlice.actions;

export default counterSlice.reducer