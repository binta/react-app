import {createSlice} from "@reduxjs/toolkit";
export const PowerSlice = createSlice({
 name: 'power',
 initialState: {value: 2},
 reducers: {
 upPower: state => {
 state.value = state.value * 2;
 }
 }
});
export const {upPower} = PowerSlice.actions;
export default PowerSlice.reducer;