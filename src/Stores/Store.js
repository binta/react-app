import {configureStore} from "@reduxjs/toolkit";
import counterReducer from './CounterSlice';
import powerSliceReducer  from "./PowerSlice";
export const store = configureStore({
 reducer: {
 counter: counterReducer,
 power: powerSliceReducer
 }
 })