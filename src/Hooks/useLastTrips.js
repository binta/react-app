import {useEffect, useState} from "react";
import { TripApiService } from "../Services/Trip/TripApiService";


export function useLastTrips() {
    const [trips, setTrips] = useState(null);

    useEffect(
        () => {
            TripApiService
                .findLastTrips()
                .then(res => {
                    const apiTrips = res.data;
                    setTrips(apiTrips);
                })
            ;
        },
        []
    );

    return trips;
}
